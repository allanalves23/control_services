-- MySQL dump 10.16  Distrib 10.1.36-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: controle_servicos
-- ------------------------------------------------------
-- Server version	10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `controle_servicos`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `controle_servicos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `controle_servicos`;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `cpf` char(15) NOT NULL,
  `rg` varchar(25) DEFAULT NULL,
  `sexo` enum('MASCULINO','FEMININO') NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cpf`),
  UNIQUE KEY `rg` (`rg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES ('caca','123','123.123.123-12','12.312.312-31','MASCULINO','sdads',0,NULL),('allan','aa','222.222.222-22','11.111.111-11','MASCULINO','dasdsa',0,NULL),('Miguel','alfredo','254.123.362-11','25.412.541-23','MASCULINO','Rua gazeta da tarde',125,NULL),('Cliente','de teste','323.225.252-15','12.547.874-51','FEMININO','teste 123',0,NULL),('Jose','da Silva','521.412.541-22','32.323.212-15','MASCULINO','rua 123',0,NULL),('Roberto','alvess','555.555.555-55','66.666.666-66','MASCULINO','rua 123',222,NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equip_trocados`
--

DROP TABLE IF EXISTS `equip_trocados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equip_trocados` (
  `sn` varchar(100) NOT NULL,
  `equipamento` varchar(100) DEFAULT NULL,
  `custo_real` decimal(5,2) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL,
  `nota` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sn`),
  KEY `id_servico` (`id_servico`),
  KEY `nota` (`nota`),
  CONSTRAINT `equip_trocados_ibfk_1` FOREIGN KEY (`id_servico`) REFERENCES `servico` (`id`),
  CONSTRAINT `equip_trocados_ibfk_2` FOREIGN KEY (`nota`) REFERENCES `nota_compra` (`registro_compra`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equip_trocados`
--

LOCK TABLES `equip_trocados` WRITE;
/*!40000 ALTER TABLE `equip_trocados` DISABLE KEYS */;
INSERT INTO `equip_trocados` VALUES ('1254','DVD',10.00,6,NULL),('215121e','Celular',850.00,8,NULL),('2512451','CD',2.50,6,NULL);
/*!40000 ALTER TABLE `equip_trocados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_compra`
--

DROP TABLE IF EXISTS `nota_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_compra` (
  `registro_compra` varchar(50) NOT NULL,
  `valor_materia` decimal(5,2) NOT NULL,
  `data_compra` date NOT NULL,
  `modo_pagamento` varchar(30) NOT NULL,
  PRIMARY KEY (`registro_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_compra`
--

LOCK TABLES `nota_compra` WRITE;
/*!40000 ALTER TABLE `nota_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servico`
--

DROP TABLE IF EXISTS `servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_servico` varchar(50) DEFAULT NULL,
  `data_recebimento` date DEFAULT NULL,
  `data_devolucao` date DEFAULT NULL,
  `prioridade` tinyint(4) DEFAULT NULL,
  `valor` decimal(5,2) DEFAULT NULL,
  `descricao` longtext,
  `nota_servico` bigint(20) DEFAULT NULL,
  `cliente` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`),
  CONSTRAINT `servico_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servico`
--

LOCK TABLES `servico` WRITE;
/*!40000 ALTER TABLE `servico` DISABLE KEYS */;
INSERT INTO `servico` VALUES (4,'CONSERTO','2018-11-11',NULL,2,10.00,'',NULL,NULL),(6,'teste123','2018-11-25','2018-11-27',5,120.52,'',NULL,'555.555.555-55'),(7,'teste','2018-11-28','2018-11-30',3,10.00,'',NULL,'222.222.222-22'),(8,'Serviço de teste','2018-12-01','2018-12-01',3,100.00,'descrição de teste',NULL,'323.225.252-15');
/*!40000 ALTER TABLE `servico` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-02 20:26:58
