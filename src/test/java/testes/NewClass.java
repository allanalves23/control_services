/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class NewClass {
    public static void main(String[] args) {
        Properties prop = new Properties();
        try {
            FileOutputStream fileOut = new FileOutputStream("cfg/config.properties");
            FileInputStream fileIn = new FileInputStream("cfg/config.properties");
            prop.load(fileIn);
            System.out.println("Info lida: "+prop.getProperty("IP_SERVER"));
            fileIn.close();
            prop.setProperty("IP_SERVER", "127.0.0.6");
            System.out.println("Info lida: "+prop.getProperty("IP_SERVER"));
            prop.store(fileOut, null);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
