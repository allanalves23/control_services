package view;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;

/**
 *
 * @author Allan
 */

public class InstanciaView {

    private static JDesktopPane fundo;
    private static JDesktopPane _fundo;
    private static JFrame config;
   
    public InstanciaView(){
        
    }
    
    public InstanciaView(JDesktopPane fundo) {
        InstanciaView.fundo = fundo;
    }

    public void abrirFrame(boolean maximizado, JInternalFrame frame) {

        if (frame.isVisible()) {
            frame.toFront();
            frame.requestFocus();
        } else {
            if (maximizado) {
                frame.setSize(fundo.getSize());
            }
            fundo.add(frame);
            frame.setVisible(true);

        }
    }
    
    public void abrirFrame(boolean maximizado, JDesktopPane fundo, JInternalFrame frame) {
        _fundo = _fundo == null ? fundo : _fundo;
        if (frame.isVisible()) {
            frame.toFront();
            frame.requestFocus();
        } else {
            if (maximizado) {
                frame.setSize(_fundo.getSize());
            }
            _fundo.add(frame);
            frame.setVisible(true);

        }
    }

    public void abrirConfig(JFrame frame) {
        if (frame.isVisible()) {
            frame.toFront();
        } else {
            frame.setVisible(true);
        }
    }
}
