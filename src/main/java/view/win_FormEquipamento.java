/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import bean.EquipamentoTrocado;
import bean.NotaCompra;
import dao.DaoEquipamentoTrocado;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan
 */
public class win_FormEquipamento extends javax.swing.JDialog {

    public win_FormEquipamento(java.awt.Frame parent, boolean modal, int id_servico) {
        super(parent, modal);
        initComponents();
        this.id_servico.setText(String.valueOf(id_servico));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_equipamento = new javax.swing.JLabel();
        equipamento = new javax.swing.JTextField();
        lbl_serial = new javax.swing.JLabel();
        serial = new javax.swing.JTextField();
        custo = new javax.swing.JTextField();
        lbl_rs = new javax.swing.JLabel();
        lbl_custo = new javax.swing.JLabel();
        incluir_nota_compra = new javax.swing.JCheckBox();
        painel_nota_compra = new javax.swing.JPanel();
        painel_nota_compra.setVisible(false);
        jLabel2 = new javax.swing.JLabel();
        registro_compra = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lbl_rs1 = new javax.swing.JLabel();
        valor_materia = new javax.swing.JTextField();
        lbl_data_compra = new javax.swing.JLabel();
        data_compra = new javax.swing.JFormattedTextField();
        modo_pagamento = new javax.swing.JComboBox<>();
        lbl_modo_pagamento = new javax.swing.JLabel();
        btn_salvar = new javax.swing.JButton();
        btn_limpar = new javax.swing.JButton();
        btn_fechar = new javax.swing.JButton();
        id_servico = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Incluir equipamento");
        setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        setType(java.awt.Window.Type.UTILITY);

        lbl_equipamento.setText("Equipamento *");

        lbl_serial.setText("Serial (Numero de série) *");

        lbl_rs.setText("R$");

        lbl_custo.setText("Custo *");

        incluir_nota_compra.setText("Incluir nota de compra?");
        incluir_nota_compra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incluir_nota_compraActionPerformed(evt);
            }
        });

        jLabel2.setText("Registro compra *");

        jLabel1.setText("Valor material *");

        lbl_rs1.setText("R$");

        lbl_data_compra.setText("Dia da compra *");

        data_compra.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        data_compra.setMinimumSize(new java.awt.Dimension(130, 25));
        data_compra.setPreferredSize(new java.awt.Dimension(130, 25));

        modo_pagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DINHEIRO CLIENTE", "DINHEIRO TÉCNICO", "CARTÃO CRÉDITO CLIENTE", "CARTÃO DÉBITO CLIENTE", "CARTÃO CRÉDTIO TÉCNICO", "CARTÃO DÉBITO TÉCNICO", "" }));

        lbl_modo_pagamento.setText("Modo de pagamento *");

        javax.swing.GroupLayout painel_nota_compraLayout = new javax.swing.GroupLayout(painel_nota_compra);
        painel_nota_compra.setLayout(painel_nota_compraLayout);
        painel_nota_compraLayout.setHorizontalGroup(
            painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painel_nota_compraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painel_nota_compraLayout.createSequentialGroup()
                        .addComponent(lbl_modo_pagamento)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(painel_nota_compraLayout.createSequentialGroup()
                        .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(modo_pagamento, javax.swing.GroupLayout.Alignment.LEADING, 0, 150, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(registro_compra, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(26, 26, 26)
                        .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(painel_nota_compraLayout.createSequentialGroup()
                                .addComponent(lbl_rs1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(valor_materia, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                            .addComponent(jLabel1))
                        .addGap(35, 35, 35)
                        .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_data_compra)
                            .addComponent(data_compra, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27))))
        );
        painel_nota_compraLayout.setVerticalGroup(
            painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painel_nota_compraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel1))
                    .addComponent(lbl_data_compra))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(painel_nota_compraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(registro_compra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(valor_materia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_rs1))
                    .addComponent(data_compra, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(23, 23, 23)
                .addComponent(lbl_modo_pagamento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(modo_pagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        btn_salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        btn_salvar.setText("Salvar");
        btn_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salvarActionPerformed(evt);
            }
        });

        btn_limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/backspace-arrow.png"))); // NOI18N
        btn_limpar.setText("Limpar");
        btn_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limparActionPerformed(evt);
            }
        });

        btn_fechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/back-arrow.png"))); // NOI18N
        btn_fechar.setText("Fechar");
        btn_fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fecharActionPerformed(evt);
            }
        });

        id_servico.setEnabled(false);

        jLabel3.setText("Serviço");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_fechar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(154, 154, 154)
                        .addComponent(btn_salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(incluir_nota_compra)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(id_servico, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(painel_nota_compra, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_equipamento)
                                    .addComponent(equipamento, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(43, 43, 43)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(serial, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbl_serial))
                                .addGap(55, 55, 55)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lbl_rs)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(custo, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                                    .addComponent(lbl_custo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(28, 28, 28))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_serial)
                            .addComponent(lbl_custo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(serial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(custo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_rs, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_equipamento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(equipamento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(incluir_nota_compra)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(id_servico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(painel_nota_compra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_salvar)
                    .addComponent(btn_limpar)
                    .addComponent(btn_fechar))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limparActionPerformed
        limparCampos();
    }//GEN-LAST:event_btn_limparActionPerformed

    private void btn_fecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fecharActionPerformed
        dispose();
    }//GEN-LAST:event_btn_fecharActionPerformed

    private void incluir_nota_compraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incluir_nota_compraActionPerformed
        if (incluir_nota_compra.isSelected()) {
            painel_nota_compra.setVisible(true);
        } else {
            painel_nota_compra.setVisible(false);
        }
        pack();
    }//GEN-LAST:event_incluir_nota_compraActionPerformed

    private boolean verificaCamposEquipamento(){
        return (equipamento.getText().equals("") || serial.getText().equals("") || custo.getText().equals(""));
    } 
    
    private boolean verificaCamposNota(){
        return (registro_compra.getText().equals("") || valor_materia.getText().equals("") || data_compra.getText().equals("") || modo_pagamento.getSelectedItem().toString().equals(""));
    }
    
    private void btn_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salvarActionPerformed
        if(incluir_nota_compra.isSelected()){
            if(verificaCamposEquipamento() || verificaCamposNota()){
                JOptionPane.showMessageDialog(this, "Todos os campos são obrigatórios para registro do produto e nota de compra"
                    + "\nVerifique os campos e tente novamente!", "Um ou mais campos em branco", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }else{
            if(verificaCamposEquipamento()){
                JOptionPane.showMessageDialog(this, "Todos os campos são obrigatórios para registro do produto"
                    + "\nVerifique os campos e tente novamente!", "Um ou mais campos em branco", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        registrarEquipamento(incluir_nota_compra.isSelected());
    }//GEN-LAST:event_btn_salvarActionPerformed

    private void registrarEquipamento(boolean nota_compra) {
        if (nota_compra) {
            try {
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                java.sql.Date data_com = new java.sql.Date(formato.parse(data_compra.getText()).getTime());
                NotaCompra nota_com = new NotaCompra(registro_compra.getText(), Float.parseFloat(valor_materia.getText()), data_com, modo_pagamento.getSelectedItem().toString());
                EquipamentoTrocado equip = new EquipamentoTrocado(serial.getText(), equipamento.getText(), Float.parseFloat(custo.getText()), Integer.parseInt(id_servico.getText()), nota_com);
                boolean acao = DaoEquipamentoTrocado.adicionarEquipamento(equip, Integer.parseInt(id_servico.getText()));
                if (acao) {
                    JOptionPane.showMessageDialog(this, "Equipamento registrado com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Ocorreu um erro ao registrar o equipamento!\nSe persistir reporte!", "Erro", JOptionPane.ERROR_MESSAGE);
                    limparCampos();
                }
            } catch (ParseException ex) {
                Logger.getLogger(win_FormEquipamento.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            EquipamentoTrocado equip = new EquipamentoTrocado(serial.getText(), equipamento.getText(), Float.parseFloat(custo.getText()), Integer.parseInt(id_servico.getText()));
            boolean acao = DaoEquipamentoTrocado.adicionarEquipamento(equip, Integer.parseInt(id_servico.getText()));
            if (acao) {
                JOptionPane.showMessageDialog(this, "Equipamento registrado com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Ocorreu um erro ao registrar o equipamento!\nSe persistir reporte!", "Erro", JOptionPane.ERROR_MESSAGE);
                limparCampos();
            }
        }
    }

    private void limparCampos() {
        equipamento.setText("");
        serial.setText("");
        custo.setText("");
        registro_compra.setText("");
        valor_materia.setText("");
        data_compra.setText("");
        modo_pagamento.setSelectedItem("DINHEIRO CLIENTE");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_fechar;
    private javax.swing.JButton btn_limpar;
    private javax.swing.JButton btn_salvar;
    private javax.swing.JTextField custo;
    private javax.swing.JFormattedTextField data_compra;
    private javax.swing.JTextField equipamento;
    private javax.swing.JTextField id_servico;
    private javax.swing.JCheckBox incluir_nota_compra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lbl_custo;
    private javax.swing.JLabel lbl_data_compra;
    private javax.swing.JLabel lbl_equipamento;
    private javax.swing.JLabel lbl_modo_pagamento;
    private javax.swing.JLabel lbl_rs;
    private javax.swing.JLabel lbl_rs1;
    private javax.swing.JLabel lbl_serial;
    private javax.swing.JComboBox<String> modo_pagamento;
    private javax.swing.JPanel painel_nota_compra;
    private javax.swing.JTextField registro_compra;
    private javax.swing.JTextField serial;
    private javax.swing.JTextField valor_materia;
    // End of variables declaration//GEN-END:variables
}
