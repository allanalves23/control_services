/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import connection.ParearConexao;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author Allan
 */
public class win_FormBackupDB extends javax.swing.JInternalFrame {

    private SwingWorker loading;
    private static win_FormBackupDB frame;
    
    public static win_FormBackupDB openFrame(){
        if(frame == null) frame = new win_FormBackupDB();
        return frame;
    }
    
    /**
     * Creates new form win_FormBackupDB
     */
    public win_FormBackupDB() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        lbl_diretorio = new javax.swing.JLabel();
        lbl_nome_arquivo = new javax.swing.JLabel();
        nome_arquivo = new javax.swing.JTextField();
        btn_procurar_dir_dump = new javax.swing.JButton();
        btn_executar_exportar = new javax.swing.JButton();
        diretorio = new javax.swing.JTextField();
        btn_fechar = new javax.swing.JButton();
        info_msg_caminho = new javax.swing.JLabel();
        info_msg_nome = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lbl_nome_arquivo1 = new javax.swing.JLabel();
        btn_procurar_arquivodump = new javax.swing.JButton();
        lbl_diretorio1 = new javax.swing.JLabel();
        btn_fechar1 = new javax.swing.JButton();
        btn_executar_importar = new javax.swing.JButton();
        btn_procurar_arquivosql = new javax.swing.JButton();
        info_msg_caminho_sql = new javax.swing.JLabel();
        info_msg_caminho_dump = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lb_estado_processamento = new javax.swing.JLabel();
        barra_loading = new javax.swing.JProgressBar();
        barra_loading.setVisible(false);
        diretorio_arquivo_sql = new javax.swing.JTextField();
        diretorio_arquivo_dump = new javax.swing.JTextField();

        setClosable(true);
        setTitle("Backup Base de dados");

        lbl_diretorio.setText("Diretório mysqldump.exe");

        lbl_nome_arquivo.setText("Nome do arquivo");

        nome_arquivo.setText("bkp_control_servicos");

        btn_procurar_dir_dump.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\magnifier.png")); // NOI18N
        btn_procurar_dir_dump.setText("Procurar");
        btn_procurar_dir_dump.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_procurar_dir_dumpActionPerformed(evt);
            }
        });

        btn_executar_exportar.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\save.png")); // NOI18N
        btn_executar_exportar.setText("Executar");
        btn_executar_exportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_executar_exportarActionPerformed(evt);
            }
        });

        btn_fechar.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\back-arrow.png")); // NOI18N
        btn_fechar.setText("Fechar");
        btn_fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fecharActionPerformed(evt);
            }
        });

        info_msg_caminho.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        info_msg_caminho.setForeground(new java.awt.Color(204, 0, 0));
        info_msg_caminho.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        info_msg_nome.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        info_msg_nome.setForeground(new java.awt.Color(204, 0, 51));
        info_msg_nome.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_fechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_executar_exportar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbl_diretorio)
                            .addComponent(lbl_nome_arquivo)
                            .addComponent(nome_arquivo)
                            .addComponent(diretorio, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_procurar_dir_dump))
                    .addComponent(info_msg_caminho, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                    .addComponent(info_msg_nome, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_nome_arquivo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nome_arquivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(info_msg_nome, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbl_diretorio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(diretorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_procurar_dir_dump))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(info_msg_caminho, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_fechar)
                    .addComponent(btn_executar_exportar))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Exportar", jPanel1);

        lbl_nome_arquivo1.setText("Arquivo de backup unificado");

        btn_procurar_arquivodump.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\magnifier.png")); // NOI18N
        btn_procurar_arquivodump.setText("Procurar");
        btn_procurar_arquivodump.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_procurar_arquivodumpActionPerformed(evt);
            }
        });

        lbl_diretorio1.setText("Diretório mysql.exe");

        btn_fechar1.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\back-arrow.png")); // NOI18N
        btn_fechar1.setText("Fechar");
        btn_fechar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fechar1ActionPerformed(evt);
            }
        });

        btn_executar_importar.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\save.png")); // NOI18N
        btn_executar_importar.setText("Executar");
        btn_executar_importar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_executar_importarActionPerformed(evt);
            }
        });

        btn_procurar_arquivosql.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\magnifier.png")); // NOI18N
        btn_procurar_arquivosql.setText("Procurar");
        btn_procurar_arquivosql.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_procurar_arquivosqlActionPerformed(evt);
            }
        });

        info_msg_caminho_sql.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        info_msg_caminho_sql.setForeground(new java.awt.Color(204, 0, 0));
        info_msg_caminho_sql.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        info_msg_caminho_dump.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        info_msg_caminho_dump.setForeground(new java.awt.Color(204, 0, 0));
        info_msg_caminho_dump.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lb_estado_processamento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lb_estado_processamento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_estado_processamento.setIcon(new javax.swing.ImageIcon("C:\\xampp\\htdocs\\control_services\\src\\main\\java\\img\\checked.png")); // NOI18N

        barra_loading.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        barra_loading.setIndeterminate(true);
        barra_loading.setName(""); // NOI18N
        barra_loading.setPreferredSize(new java.awt.Dimension(100, 14));
        barra_loading.setString("");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lb_estado_processamento, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barra_loading, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lb_estado_processamento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barra_loading, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(info_msg_caminho_sql, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(diretorio_arquivo_sql, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                                .addComponent(btn_procurar_arquivosql))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(info_msg_caminho_dump, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(diretorio_arquivo_dump, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn_fechar1)
                        .addComponent(lbl_diretorio1)
                        .addComponent(lbl_nome_arquivo1))
                    .addGap(71, 71, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_procurar_arquivodump, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_executar_importar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_procurar_arquivosql)
                    .addComponent(diretorio_arquivo_sql, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(info_msg_caminho_sql, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(diretorio_arquivo_dump, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(info_msg_caminho_dump, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lbl_nome_arquivo1)
                    .addGap(66, 66, 66)
                    .addComponent(lbl_diretorio1)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btn_procurar_arquivodump)
                    .addGap(37, 37, 37)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_fechar1)
                        .addComponent(btn_executar_importar))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Importar", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Exportar");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_procurar_dir_dumpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_procurar_dir_dumpActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (fileChooser.showOpenDialog(null) == 1) {
            diretorio.setText("");
        } else {
            File arquivo = fileChooser.getSelectedFile();
            diretorio.setText(arquivo.getPath());
        }

    }//GEN-LAST:event_btn_procurar_dir_dumpActionPerformed

    private void btn_fecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fecharActionPerformed
        dispose();
    }//GEN-LAST:event_btn_fecharActionPerformed

    private void btn_executar_exportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_executar_exportarActionPerformed
        if (nome_arquivo.getText().length() < 3) {
            info_msg_nome.setText("Nome do arquivo deve possuir pelo menos 3 caracteres");
            return;
        }

        if (nome_arquivo.getText().contains(".")) {
            info_msg_nome.setText("Nome do arquivo não pode possuir o caractere . (ponto)");
            return;
        }

        if (!diretorio.getText().contains("mysqldump.exe")) {
            info_msg_caminho.setText("Arquivo mysqldump.exe não encontrado");
            return;
        }

        try {
            Runtime.getRuntime().exec(diretorio.getText() + " -v -v -v --host=" + ParearConexao.conectar().getHost() + " --user=root --port=3306 --protocol=tcp --force --allow-keywords --compress --add-drop-table --result-file=" + nome_arquivo.getText() + ".sql --databases controle_servicos");
            JOptionPane.showMessageDialog(this, "Backup feito com sucesso", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
            Runtime.getRuntime().exec("explorer.exe .");
        } catch (IOException ex) {
            Logger.getLogger(win_FormBackupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btn_executar_exportarActionPerformed

    private void btn_procurar_arquivodumpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_procurar_arquivodumpActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (fileChooser.showOpenDialog(null) == 1) {
            diretorio_arquivo_dump.setText("");
        } else {
            File arquivo = fileChooser.getSelectedFile();
            diretorio_arquivo_dump.setText(arquivo.getPath());
        }

    }//GEN-LAST:event_btn_procurar_arquivodumpActionPerformed

    private void btn_fechar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fechar1ActionPerformed
        dispose();
    }//GEN-LAST:event_btn_fechar1ActionPerformed

    private void btn_executar_importarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_executar_importarActionPerformed

        if (!diretorio_arquivo_sql.getText().contains(".sql")) {
            info_msg_caminho_sql.setText("Arquvio .sql não encontrado");
            return;
        }

        if (!diretorio_arquivo_dump.getText().contains("mysql.exe")) {
            info_msg_caminho_dump.setText("Arquivo mysql.exe não encontrado");
            return;
        }

        if (JOptionPane.showConfirmDialog(this, "Efetuar a importação irá sobrescrever todos os dados armazenados\nPressione NÃO para cancelar\nPressione SIM para efetuar a importação", "ATENÇÃO", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == 0) {

            loading = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    lb_estado_processamento.setText("Processando!");
                    barra_loading.setVisible(true);
                    Process proc = Runtime.getRuntime().exec("cmd /c " + diretorio_arquivo_dump.getText() + " --protocol=tcp --host=" + ParearConexao.conectar().getHost() + " --user=root --port=3306 --default-character-set=utf8 --comments --database=controle_servicos < " + diretorio_arquivo_sql.getText());
                    return 0;
                }
            };
            SwingWorker.StateValue estado = SwingWorker.StateValue.PENDING;
            loading.execute();
            while (!loading.isDone()) {
                estado = SwingWorker.StateValue.STARTED;
            }
            if (loading.isDone()) {
                estado = SwingWorker.StateValue.DONE;
            }

            if (loading.isCancelled()) {
                lb_estado_processamento.setText("Ocorreu um erro ao importar, se persistir reporte!");
                barra_loading.setVisible(false);
            }

            if (estado == SwingWorker.StateValue.DONE) {
                lb_estado_processamento.setText("Banco importado com sucesso!");
                barra_loading.setVisible(false);
            }
        }

    }//GEN-LAST:event_btn_executar_importarActionPerformed

    private void btn_procurar_arquivosqlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_procurar_arquivosqlActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (fileChooser.showOpenDialog(null) == 1) {
            diretorio_arquivo_sql.setText("");
        } else {
            File arquivo = fileChooser.getSelectedFile();
            diretorio_arquivo_sql.setText(arquivo.getPath());
        }
    }//GEN-LAST:event_btn_procurar_arquivosqlActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra_loading;
    private javax.swing.JButton btn_executar_exportar;
    private javax.swing.JButton btn_executar_importar;
    private javax.swing.JButton btn_fechar;
    private javax.swing.JButton btn_fechar1;
    private javax.swing.JButton btn_procurar_arquivodump;
    private javax.swing.JButton btn_procurar_arquivosql;
    private javax.swing.JButton btn_procurar_dir_dump;
    private javax.swing.JTextField diretorio;
    private javax.swing.JTextField diretorio_arquivo_dump;
    private javax.swing.JTextField diretorio_arquivo_sql;
    private javax.swing.JLabel info_msg_caminho;
    private javax.swing.JLabel info_msg_caminho_dump;
    private javax.swing.JLabel info_msg_caminho_sql;
    private javax.swing.JLabel info_msg_nome;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lb_estado_processamento;
    private javax.swing.JLabel lbl_diretorio;
    private javax.swing.JLabel lbl_diretorio1;
    private javax.swing.JLabel lbl_nome_arquivo;
    private javax.swing.JLabel lbl_nome_arquivo1;
    private javax.swing.JTextField nome_arquivo;
    // End of variables declaration//GEN-END:variables
}
