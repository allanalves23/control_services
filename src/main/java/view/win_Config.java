/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import connection.ParearConexao;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan
 */
public class win_Config extends javax.swing.JDialog {

    /**
     * Creates new form win_Config
     *
     * @param parent
     * @param modal
     */
    InstanciaView view_instance;

    public win_Config(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        verificarSenhaExistente();
        verificarConexão();
        view_instance = new InstanciaView();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fundo = new javax.swing.JDesktopPane();
        fundo = new javax.swing.JDesktopPane(){
            public void paintComponent(Graphics g){
                g.setColor(new java.awt.Color(69, 86, 152));
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menu_item_criar_senha = new javax.swing.JMenuItem();
        menu_item_senha = new javax.swing.JMenuItem();
        menu_item_remover_fpsw = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        menu_item_config = new javax.swing.JMenuItem();
        menu_item_exportar = new javax.swing.JMenuItem();
        menu_item_info = new javax.swing.JMenuItem();
        menu_item_reiniciar = new javax.swing.JMenuItem();
        menu_item_criar_db = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        menu_item_abrir_notas = new javax.swing.JMenuItem();
        menu_temas = new javax.swing.JMenu();
        menu_item_padrao = new javax.swing.JMenuItem();
        menu_item_windows = new javax.swing.JMenuItem();
        menu_item_classic = new javax.swing.JMenuItem();
        menu_item_sair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Configurações");
        setAlwaysOnTop(true);
        setName("win_Config"); // NOI18N
        setType(java.awt.Window.Type.UTILITY);

        fundo.setBackground(new java.awt.Color(69, 86, 152));
        fundo.setPreferredSize(new java.awt.Dimension(551, 420));

        javax.swing.GroupLayout fundoLayout = new javax.swing.GroupLayout(fundo);
        fundo.setLayout(fundoLayout);
        fundoLayout.setHorizontalGroup(
            fundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 535, Short.MAX_VALUE)
        );
        fundoLayout.setVerticalGroup(
            fundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 428, Short.MAX_VALUE)
        );

        jMenuBar1.setPreferredSize(new java.awt.Dimension(169, 25));

        jMenu1.setText("Conta");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jMenu1MouseReleased(evt);
            }
        });

        menu_item_criar_senha.setText("Criar senha");
        File file = new File("cfg/fpsw.dat");
        if(file.exists()){
            menu_item_criar_senha.setEnabled(false);
        }

        menu_item_criar_senha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_criar_senhaActionPerformed(evt);
            }
        });
        jMenu1.add(menu_item_criar_senha);

        menu_item_senha.setText("Alterar Senha");
        file = new File("cfg/fpsw.dat");
        if(!file.exists()){
            menu_item_senha.setEnabled(false);
        }
        menu_item_senha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_senhaActionPerformed(evt);
            }
        });
        jMenu1.add(menu_item_senha);

        menu_item_remover_fpsw.setText("Remover fpsw.dat");
        file = new File("cfg/fpsw.dat");
        if(!file.exists()){
            menu_item_remover_fpsw.setEnabled(false);
        }

        menu_item_remover_fpsw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_remover_fpswActionPerformed(evt);
            }
        });
        jMenu1.add(menu_item_remover_fpsw);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Base de dados");

        jMenu4.setText("SGBD");

        menu_item_config.setText("Configurar argumentos de acesso");
        menu_item_config.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_configActionPerformed(evt);
            }
        });
        jMenu4.add(menu_item_config);

        menu_item_exportar.setText("Importar/Exportar Base de dados");
        menu_item_exportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_exportarActionPerformed(evt);
            }
        });
        jMenu4.add(menu_item_exportar);

        menu_item_info.setText("Informações");
        menu_item_info.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_infoActionPerformed(evt);
            }
        });
        jMenu4.add(menu_item_info);

        menu_item_reiniciar.setText("Reiniciar Serviço");
        menu_item_reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_reiniciarActionPerformed(evt);
            }
        });
        jMenu4.add(menu_item_reiniciar);

        menu_item_criar_db.setText("Criar base de dados");
        menu_item_criar_db.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_criar_dbActionPerformed(evt);
            }
        });
        jMenu4.add(menu_item_criar_db);

        jMenu2.add(jMenu4);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Outros");

        menu_item_abrir_notas.setText("Abrir notas de serviço existentes");
        menu_item_abrir_notas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_abrir_notasActionPerformed(evt);
            }
        });
        jMenu3.add(menu_item_abrir_notas);

        menu_temas.setText("Temas");

        menu_item_padrao.setText("Padrão");
        menu_item_padrao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_padraoActionPerformed(evt);
            }
        });
        menu_temas.add(menu_item_padrao);

        menu_item_windows.setText("Windows");
        menu_item_windows.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_windowsActionPerformed(evt);
            }
        });
        menu_temas.add(menu_item_windows);

        menu_item_classic.setText("Classic");
        menu_item_classic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_classicActionPerformed(evt);
            }
        });
        menu_temas.add(menu_item_classic);

        jMenu3.add(menu_temas);

        menu_item_sair.setText("Fechar");
        menu_item_sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_item_sairActionPerformed(evt);
            }
        });
        jMenu3.add(menu_item_sair);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fundo, javax.swing.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fundo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menu_item_criar_senhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_criar_senhaActionPerformed
        view_instance.abrirFrame(false, fundo, win_AlterarSenha.openFrame(menu_item_criar_senha, menu_item_senha, menu_item_remover_fpsw));
//        win_AlterarSenha alt_senha = new win_AlterarSenha(menu_item_criar_senha, menu_item_senha, menu_item_remover_fpsw);
//        fundo.add(alt_senha);
//        alt_senha.setVisible(true);
    }//GEN-LAST:event_menu_item_criar_senhaActionPerformed

    private void menu_item_senhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_senhaActionPerformed
        view_instance.abrirFrame(false, fundo, win_AlterarSenha.openFrame(menu_item_criar_senha, menu_item_senha, menu_item_remover_fpsw));
//        win_AlterarSenha alt_senha = new win_AlterarSenha(menu_item_criar_senha, menu_item_senha, menu_item_remover_fpsw);
//        fundo.add(alt_senha);
//        alt_senha.setVisible(true);
    }//GEN-LAST:event_menu_item_senhaActionPerformed

    private void menu_item_remover_fpswActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_remover_fpswActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja remover o arquivo?", "Tem certeza?", JOptionPane.YES_NO_OPTION) == 0) {
            File file = new File("cfg/fpsw.dat");
            if (file.exists()) {
                file.delete();
                JOptionPane.showMessageDialog(this, "Arquivo removido com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                verificarSenhaExistente();
            } else {
                JOptionPane.showMessageDialog(this, "O arquivo não existe!", "Aviso!", JOptionPane.WARNING_MESSAGE);
            }
        }
        verificarSenhaExistente();
    }//GEN-LAST:event_menu_item_remover_fpswActionPerformed

    private void menu_item_configActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_configActionPerformed
        view_instance.abrirFrame(false, fundo, win_FormConfigDB.openFrame());
    }//GEN-LAST:event_menu_item_configActionPerformed

    private void menu_item_exportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_exportarActionPerformed
        view_instance.abrirFrame(false, fundo, win_FormConfigDB.openFrame());
    }//GEN-LAST:event_menu_item_exportarActionPerformed

    private void menu_item_infoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_infoActionPerformed
        String ip = "SEM CONEXÃO";
        String lh = "NÃO";
        String st = "OFFLINE";

        if (ParearConexao.conectar() != null) {
            st = "ONLINE";
            ip = ParearConexao.conectar().getHost();
            if (ip.contains("127.0.0")) {
                lh = "SIM";
            }
        }
        JOptionPane.showMessageDialog(this, "SGBD: Mysql/MariaDB\nIP: " + ip + "\nLocalHost: " + lh + "\nStatus: " + st, "Aviso!", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_menu_item_infoActionPerformed

    private void menu_item_reiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_reiniciarActionPerformed
        ParearConexao.fechar();
        ParearConexao.conectar();
        JOptionPane.showMessageDialog(this, "Conexão reiniciada com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_menu_item_reiniciarActionPerformed

    private void menu_item_criar_dbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_criar_dbActionPerformed
        /* retornos:
               0 = Banco de dados ja existe
              -1 = Erro ao criar as tabelas
               1 = Sem erros!
               2 = Exception MYSQL
         */
        switch (criarBase()) {
            case -1: {
                JOptionPane.showMessageDialog(this, "Ocorreu um erro na criação de tabelas\nExclua a base de dados atual ou\nrealize uma importação", "Aviso", JOptionPane.WARNING_MESSAGE);
                break;
            }
            case 0: {
                JOptionPane.showMessageDialog(this, "Banco de dados ja existe!", "Aviso", JOptionPane.WARNING_MESSAGE);
                break;
            }
            case 1: {
                JOptionPane.showMessageDialog(this, "Base de dados criada com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                break;
            }
            case 2: {
                JOptionPane.showMessageDialog(this, "Ocorreu um erro ao realizar a comunicação com o servidor Mysql\nReinicie o servidor e tente novamente\nse persistir reporte!", "Erro!", JOptionPane.ERROR_MESSAGE);
                break;
            }
        }
        verificarConexão();
    }//GEN-LAST:event_menu_item_criar_dbActionPerformed

    private void menu_item_abrir_notasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_abrir_notasActionPerformed
        File file = new File("notas_servico");
        if (file.exists()) {
            try {
                Runtime.getRuntime().exec("explorer.exe " + file.getCanonicalPath());
            } catch (IOException ex) {
                Logger.getLogger(win_Config.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Não existem notas de serviços no sistema");
        }
    }//GEN-LAST:event_menu_item_abrir_notasActionPerformed

    private void menu_item_padraoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_padraoActionPerformed
        alterarTema("Nimbus");
    }//GEN-LAST:event_menu_item_padraoActionPerformed

    private void menu_item_windowsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_windowsActionPerformed
        alterarTema("Windows");
    }//GEN-LAST:event_menu_item_windowsActionPerformed

    private void menu_item_classicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_classicActionPerformed
        alterarTema("Windows Classic");
    }//GEN-LAST:event_menu_item_classicActionPerformed

    private void menu_item_sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_item_sairActionPerformed
        dispose();
    }//GEN-LAST:event_menu_item_sairActionPerformed

    private void jMenu1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseReleased
        verificarSenhaExistente();
    }//GEN-LAST:event_jMenu1MouseReleased

    private void alterarTema(String tema) {
        if (javax.swing.UIManager.getLookAndFeel().getName().equals(tema)) {
            JOptionPane.showMessageDialog(this, "Este tema já está aplicado!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja aplicar o tema '" + tema + "'?", "Tem certeza?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
            if (file.Tema.gravarTema(tema)) {
                JOptionPane.showMessageDialog(this, "Tema gravado com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                try {
                    Runtime.getRuntime().exec("start.bat");
                } catch (IOException ex) {
                    Logger.getLogger(win_Config.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.exit(1);
            } else {
                JOptionPane.showMessageDialog(this, "Um erro ocorreu, se persistir reporte!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }

        }
    }

    private int criarBase() {
        try {
            String data_base = "CREATE DATABASE IF NOT EXISTS `controle_servicos` /*!40100 DEFAULT CHARACTER SET latin1 */;";
            /* retornos:
               0 = Banco de dados ja existe
              -1 = Erro ao criar as tabelas
               1 = Sem erros!
               2 = Exception MYSQL
             */

            try (Statement stm = ParearConexao.conectarSemBanco().createStatement()) {
                if (stm.executeUpdate(data_base) == 0) {
                    return 0;
                }
            }

            if (criarTabelaCliente() == 0 && criarTabelaNotaCompra() == 0
                    && criarTabelaServico() == 0 && criarEquipamentoTrocado() == 0) {
                return 1;
            } else {
                return -1;
            }

        } catch (SQLException ex) {
            Logger.getLogger(win_Config.class.getName()).log(Level.SEVERE, null, ex);
            return 2;
        }
    }

    private int criarTabelaCliente() {
        try {
            String tb_cliente = "CREATE TABLE `cliente` (\n"
                    + "  `nome` varchar(50) NOT NULL,\n"
                    + "  `sobrenome` varchar(100) NOT NULL,\n"
                    + "  `cpf` char(15) NOT NULL,\n"
                    + "  `rg` varchar(25) DEFAULT NULL,\n"
                    + "  `sexo` enum('MASCULINO','FEMININO') NOT NULL,\n"
                    + "  `endereco` varchar(100) NOT NULL,\n"
                    + "  `numero` int(11) DEFAULT NULL,\n"
                    + "  `complemento` varchar(100) DEFAULT NULL,\n"
                    + "  PRIMARY KEY (`cpf`),\n"
                    + "  UNIQUE KEY `rg` (`rg`)\n"
                    + ") ENGINE=InnoDB";

            int retorno;
            try (Statement stm = ParearConexao.conectar().createStatement()) {
                retorno = stm.executeUpdate(tb_cliente);
            }
            return retorno;

        } catch (SQLException ex) {
            Logger.getLogger(win_Config.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private int criarTabelaNotaCompra() {
        try {
            String nota = "CREATE TABLE `nota_compra` (\n"
                    + "  `registro_compra` varchar(50) NOT NULL,\n"
                    + "  `valor_materia` decimal(5,2) NOT NULL,\n"
                    + "  `data_compra` date NOT NULL,\n"
                    + "  `modo_pagamento` varchar(30) NOT NULL,\n"
                    + "  PRIMARY KEY (`registro_compra`)\n"
                    + ") ENGINE=InnoDB";

            int retorno;
            try (Statement stm = ParearConexao.conectar().createStatement()) {
                retorno = stm.executeUpdate(nota);
            }

            return retorno;

        } catch (SQLException ex) {
            Logger.getLogger(win_Config.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private int criarTabelaServico() {
        try {
            String servico = "CREATE TABLE `servico` (\n"
                    + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
                    + "  `tipo_servico` varchar(50) DEFAULT NULL,\n"
                    + "  `data_recebimento` date DEFAULT NULL,\n"
                    + "  `data_devolucao` date DEFAULT NULL,\n"
                    + "  `prioridade` tinyint(4) DEFAULT NULL,\n"
                    + "  `valor` decimal(5,2) DEFAULT NULL,\n"
                    + "  `descricao` longtext,\n"
                    + "  `nota_servico` bigint(20) DEFAULT NULL,\n"
                    + "  `cliente` char(15) DEFAULT NULL,\n"
                    + "  PRIMARY KEY (`id`),\n"
                    + "  KEY `cliente` (`cliente`),\n"
                    + "  CONSTRAINT `servico_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`cpf`)\n"
                    + ") ENGINE=InnoDB";

            int retorno;
            try (Statement stm = ParearConexao.conectar().createStatement()) {
                retorno = stm.executeUpdate(servico);
            }
            return retorno;

        } catch (SQLException ex) {
            Logger.getLogger(win_Config.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private int criarEquipamentoTrocado() {
        try {
            String tb_equip = "CREATE TABLE `equip_trocados` (\n"
                    + "  `sn` varchar(100) NOT NULL,\n"
                    + "  `equipamento` varchar(100) DEFAULT NULL,\n"
                    + "  `custo_real` decimal(5,2) DEFAULT NULL,\n"
                    + "  `id_servico` int(11) DEFAULT NULL,\n"
                    + "  `nota` varchar(50) DEFAULT NULL,\n"
                    + "  PRIMARY KEY (`sn`),\n"
                    + "  KEY `id_servico` (`id_servico`),\n"
                    + "  KEY `nota` (`nota`),\n"
                    + "  CONSTRAINT `equip_trocados_ibfk_1` FOREIGN KEY (`id_servico`) REFERENCES `servico` (`id`),\n"
                    + "  CONSTRAINT `equip_trocados_ibfk_2` FOREIGN KEY (`nota`) REFERENCES `nota_compra` (`registro_compra`) ON DELETE CASCADE\n"
                    + ") ENGINE=InnoDB";

            int retorno;
            try (Statement stm = ParearConexao.conectar().createStatement()) {
                retorno = stm.executeUpdate(tb_equip);
            }

            return retorno;

        } catch (SQLException ex) {
            Logger.getLogger(win_Config.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private void verificarConexão() {
        if (ParearConexao.conectar() == null) {
            if (ParearConexao.conectarSemBanco() == null) {
                JOptionPane.showMessageDialog(this, "Para utilizar a aplicação é necessário instalar o MYSQL", "Aviso!", JOptionPane.WARNING_MESSAGE);
            } else {
                menu_item_reiniciar.setEnabled(false);
                menu_item_info.setEnabled(false);
                menu_item_abrir_notas.setEnabled(false);
                menu_temas.setEnabled(false);
            }
        } else {
            menu_item_reiniciar.setEnabled(true);
            menu_item_info.setEnabled(true);
            menu_item_abrir_notas.setEnabled(true);
            menu_temas.setEnabled(true);
        }
    }

    private void verificarSenhaExistente() {
        File file = new File("cfg/fpsw.dat");
        if (file.exists()) {
            menu_item_criar_senha.setEnabled(false);
            menu_item_senha.setEnabled(true);
            menu_item_remover_fpsw.setEnabled(true);
        } else {
            menu_item_criar_senha.setEnabled(true);
            menu_item_senha.setEnabled(false);
            menu_item_remover_fpsw.setEnabled(false);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane fundo;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem menu_item_abrir_notas;
    private javax.swing.JMenuItem menu_item_classic;
    private javax.swing.JMenuItem menu_item_config;
    private javax.swing.JMenuItem menu_item_criar_db;
    private javax.swing.JMenuItem menu_item_criar_senha;
    private javax.swing.JMenuItem menu_item_exportar;
    private javax.swing.JMenuItem menu_item_info;
    private javax.swing.JMenuItem menu_item_padrao;
    private javax.swing.JMenuItem menu_item_reiniciar;
    private javax.swing.JMenuItem menu_item_remover_fpsw;
    private javax.swing.JMenuItem menu_item_sair;
    private javax.swing.JMenuItem menu_item_senha;
    private javax.swing.JMenuItem menu_item_windows;
    private javax.swing.JMenu menu_temas;
    // End of variables declaration//GEN-END:variables
}
