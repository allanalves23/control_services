/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.EquipamentoTrocado;
import bean.NotaCompra;
import bean.Servico;
import connection.ParearConexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class DaoEquipamentoTrocado {

    public static EquipamentoTrocado obterEquipamento(EquipamentoTrocado equip) {
        String SQL = "SELECT * FROM equip_trocados WHERE sn = ?";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, equip.getSn());

            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                EquipamentoTrocado e = new EquipamentoTrocado(rs.getString("sn"), rs.getString("equipamento"), rs.getFloat("custo_real"), rs.getInt("id_servico"));
                return e;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new EquipamentoTrocado();
    }
    
    public static boolean adicionarEquipamento(EquipamentoTrocado equipamento, int id_servico) {
        if (equipamento.getNota_compra() == null) {
            String SQL = "INSERT INTO equip_trocados (sn, equipamento, custo_real, id_servico) VALUES (?,?,?,?)";
            try {
                PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, equipamento.getSn());
                pstm.setString(2, equipamento.getEquipamento());
                pstm.setFloat(3, equipamento.getCusto_real());
                pstm.setInt(4, equipamento.getId_servico());

                if (pstm.executeUpdate() == 1) {
                    ParearConexao.fechar();
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException ex) {
                Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            String SQL = "INSERT INTO nota_compra (registro_compra, valor_materia, data_compra, modo_pagamento) VALUES (?,?,?,?)";
            try {
                PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, equipamento.getNota_compra().getRegistro_compra());
                pstm.setFloat(2, equipamento.getNota_compra().getValor_materia());
                pstm.setDate(3, equipamento.getNota_compra().getData_compra());
                pstm.setString(4, equipamento.getNota_compra().getModo_pagamento());

                if (pstm.executeUpdate() == 1) {
                    SQL = "INSERT INTO equip_trocados (sn, equipamento, custo_real, id_servico, nota) VALUES (?,?,?,?,?)";
                    pstm = ParearConexao.conectar().prepareStatement(SQL);
                    pstm.setString(1, equipamento.getSn());
                    pstm.setString(2, equipamento.getEquipamento());
                    pstm.setFloat(3, equipamento.getCusto_real());
                    pstm.setInt(4, equipamento.getId_servico());
                    pstm.setString(5, equipamento.getNota_compra().getRegistro_compra());

                    if (pstm.executeUpdate() == 1) {
                        ParearConexao.fechar();
                        return true;
                    } else {
                        SQL = "DELETE FROM nota_compra WHERE registro_compra = ?";
                        pstm = ParearConexao.conectar().prepareStatement(SQL);
                        pstm.setString(1, equipamento.getNota_compra().getRegistro_compra());
                        pstm.executeUpdate();
                        ParearConexao.fechar();
                        return false;
                    }

                }
            } catch (SQLException ex) {
                Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public static int removerEquipamento(EquipamentoTrocado equipamento) {
        String SQL = "DELETE FROM equip_trocados WHERE sn = ?";
        int result = 0;
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, equipamento.getSn());
            result = pstm.executeUpdate();
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static NotaCompra obterNotaDeCompra(String sn) {
        String SQL = "SELECT et.equipamento, et.sn, nc.registro_compra, "
                + "nc.valor_materia, nc.data_compra, nc.modo_pagamento FROM "
                + "nota_compra nc join equip_trocados et on nc.registro_compra = et.nota "
                + "WHERE et.sn = ?";

        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, sn);

            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                NotaCompra nc = new NotaCompra(rs.getString("registro_compra"), rs.getFloat("valor_materia"), rs.getDate("data_compra"),
                        rs.getString("modo_pagamento"), rs.getString("equipamento"), rs.getString("sn"));
                return nc;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new NotaCompra();
    }

    public static int adicionarNotaCompra(NotaCompra nota_compra) {
        String SQL = "INSERT INTO nota_compra VALUES (?,?,?,?)";
        int opcao = 0;
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, nota_compra.getRegistro_compra());
            pstm.setFloat(2, nota_compra.getValor_materia());
            pstm.setDate(3, nota_compra.getData_compra());
            pstm.setString(4, nota_compra.getModo_pagamento());

            if (pstm.executeUpdate() == 1) {
                SQL = "UPDATE equip_trocados SET nota = ? WHERE sn = ?";
                pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, nota_compra.getRegistro_compra());
                pstm.setString(2, nota_compra.getSn());
                opcao = pstm.executeUpdate();
                ParearConexao.fechar();
                return opcao;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return opcao;
    }

    public static int removerNotaCompra(String nota) {
        String SQL = "DELETE FROM nota_compra WHERE registro_compra = ?";
        int result = 0;
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, nota);
            result = pstm.executeUpdate();
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static int removerNotaCompra(NotaCompra nota) {
        int result = 0;
        String SQL = "UPDATE equip_trocados SET nota = NULL WHERE sn = ?";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, nota.getSn());
            if (pstm.executeUpdate() == 1) {
                SQL = "DELETE FROM nota_compra WHERE registro_compra = ?";
                pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, nota.getRegistro_compra());
                result = pstm.executeUpdate();
                ParearConexao.fechar();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoEquipamentoTrocado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
