/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.Cliente;
import connection.ParearConexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class DaoCliente {

    public static List<Cliente> visualizarClientes() throws SQLException {
        String SQL = "SELECT * FROM cliente";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            ResultSet rs = pstm.executeQuery();
            List<Cliente> clientes = new ArrayList<>();
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setNome(rs.getString("nome"));
                cliente.setSobrenome(rs.getString("sobrenome"));
                cliente.setCpf(rs.getString("cpf"));
                cliente.setRg(rs.getString("rg"));
                cliente.setSexo(rs.getString("sexo"));
                cliente.setEndereco(rs.getString("endereco"));
                cliente.setNumero(rs.getInt("numero"));
                cliente.setComplemento(rs.getString("complemento"));
                clientes.add(cliente);
            }

            if (clientes.isEmpty()) {
                return null;
            } else {
                return clientes;
            }

        } catch (SQLException ex) {
            return null;
        }
    }

    public static boolean removerCliente(String cpf) {
        String SQL = "DELETE FROM cliente WHERE cpf = ?";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, cpf);
            return (pstm.executeUpdate() == 1);

        } catch (SQLException ex) {
            return false;
        }
    }

    public static int atualizarCliente(Cliente cliente) throws SQLException {
        String SQL = "UPDATE cliente SET nome = ?, sobrenome = ?, rg = ?, sexo = ?, endereco = ?, numero = ?, complemento = ?"
                + " WHERE cpf = ?";
        PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
        pstm.setString(1, cliente.getNome());
        pstm.setString(2, cliente.getSobrenome());
        pstm.setString(3, cliente.getRg());
        pstm.setString(4, cliente.getSexo());
        pstm.setString(5, cliente.getEndereco());
        pstm.setInt(6, cliente.getNumero());
        pstm.setString(7, cliente.getComplemento());
        pstm.setString(8, cliente.getCpf());
        int retorno = pstm.executeUpdate();
        ParearConexao.fechar();
        return retorno;
    }
    
    public static int adicionarCliente(Cliente cliente){
        
        if(cliente.getNumero()<0){
            cliente.setNumero(0);
        }
        
        if(cliente.getComplemento().isEmpty()){
            cliente.setComplemento(null);
        }
        
        String SQL = "INSERT INTO cliente VALUES (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, cliente.getNome());
            pstm.setString(2, cliente.getSobrenome());
            pstm.setString(3, cliente.getCpf());
            pstm.setString(4, cliente.getRg());
            pstm.setString(5, cliente.getSexo());
            pstm.setString(6, cliente.getEndereco());
            pstm.setInt(7, cliente.getNumero());
            pstm.setString(8, cliente.getComplemento());
            
            int rs = pstm.executeUpdate();
            ParearConexao.fechar();
            return rs;
            
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
    }
}
