/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.EquipamentoTrocado;
import bean.Servico;
import connection.ParearConexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class DaoServico {

    public static int inserirServico(Servico servico) {
        String SQL = "INSERT INTO servico (tipo_servico, data_recebimento, prioridade, valor, descricao, cliente) VALUES (?,?,?,?,?,?)";
        int resultado = 0;
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setString(1, servico.getTipo_servico());
            pstm.setDate(2, servico.getData_recebimento());
            pstm.setInt(3, servico.getPrioridade());
            pstm.setFloat(4, servico.getValor());
            pstm.setString(5, servico.getDescricao());
            pstm.setString(6, servico.getCpf_cliente());

            resultado = pstm.executeUpdate();
            return resultado;

        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }

    public static List<bean.Servico> visualizarServicos() {
        String SQL = "SELECT s.id, s.tipo_servico, c.nome, s.data_recebimento, s.data_devolucao, s.valor FROM servico s join cliente c on c.cpf = s.cliente Order by s.data_recebimento desc";
        List<bean.Servico> servicos = new ArrayList<>();

        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Servico servico = new Servico();
                servico.setId(rs.getInt("id"));
                servico.setTipo_servico(rs.getString("tipo_servico"));
                servico.setNome_cliente(rs.getString("nome"));
                servico.setData_recebimento(rs.getDate("data_recebimento"));
                servico.setData_devolucao(rs.getDate("data_devolucao"));
                servico.setValor(Float.parseFloat(rs.getString("valor")));
                servicos.add(servico);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return servicos;
    }

    public static Servico carregarServico(int id) {
        String SQL = "SELECT s.tipo_servico, s.data_recebimento, s.data_devolucao, s.prioridade,"
                + " s.valor, s.descricao, s. nota_servico, s.cliente, s.id , c.nome"
                + " FROM servico s join cliente c on c.cpf = s.cliente WHERE s.id = " + id;
        try {
            Statement pstm = ParearConexao.conectar().createStatement();
            ResultSet rs = pstm.executeQuery(SQL);
            if (rs.next()) {
                Servico servico = new Servico(rs.getString("tipo_servico"), rs.getDate("data_recebimento"),
                        rs.getDate("data_devolucao"), rs.getInt("prioridade"), rs.getFloat("valor"),
                        rs.getString("descricao"), rs.getInt("nota_servico"), rs.getString("cliente"), rs.getInt("id"), rs.getString("nome"));
                return servico;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Servico();
    }

    public static int deletarServico(int id) {
        String SQL = "DELETE FROM servico WHERE id = ?";
        int resultado = 0;
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setInt(1, id);
            resultado = pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;
    }

    public static int atualizarServico(Servico servico, int id) {
        String SQL;
        int resultado;
        if (servico.getData_devolucao() == null) {
            SQL = "UPDATE servico SET tipo_servico = ?, data_recebimento = ?, data_devolucao = NULL,"
                    + " prioridade = ?, valor = ?, descricao = ? WHERE id = ?";
            try {
                PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, servico.getTipo_servico());
                pstm.setDate(2, servico.getData_recebimento());
                pstm.setInt(3, servico.getPrioridade());
                pstm.setFloat(4, servico.getValor());
                pstm.setString(5, servico.getDescricao());
                pstm.setInt(6, id);
                resultado = pstm.executeUpdate();
                return resultado;
            } catch (SQLException ex) {
                Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
            }

            return -1;
        } else {
            SQL = "UPDATE servico SET tipo_servico = ?, data_recebimento = ?, data_devolucao = ?,"
                    + " prioridade = ?, valor = ?, descricao = ? WHERE id = ?";
            try {
                PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
                pstm.setString(1, servico.getTipo_servico());
                pstm.setDate(2, servico.getData_recebimento());
                pstm.setDate(3, servico.getData_devolucao());
                pstm.setInt(4, servico.getPrioridade());
                pstm.setFloat(5, servico.getValor());
                pstm.setString(6, servico.getDescricao());
                pstm.setInt(7, id);
                resultado = pstm.executeUpdate();
                return resultado;
            } catch (SQLException ex) {
                Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
            }

            return -1;
        }
    }

    public static List<EquipamentoTrocado> visualizarEquipamentos(int id) {
        String SQL = "SELECT equipamento, sn, custo_real, id_servico FROM equip_trocados WHERE id_servico = ?";
        List<EquipamentoTrocado> equipamentos = new ArrayList<>();
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setInt(1, id);

            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                EquipamentoTrocado equipamento = new EquipamentoTrocado(rs.getString("sn"),
                        rs.getString("equipamento"), rs.getFloat("custo_real"),
                        rs.getInt("id_servico"));
                equipamentos.add(equipamento);
            }

            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }

        return equipamentos;
    }

    public static List<EquipamentoTrocado> visualizarEquipamentos(int id, String busca) {
        String SQL = "SELECT equipamento, sn, custo_real, id_servico FROM equip_trocados WHERE id_servico = ? and (equipamento LIKE '%" + busca + "%' OR custo_real LIKE '%" + busca + "%' OR sn LIKE '%" + busca + "%')";
        List<EquipamentoTrocado> equipamentos = new ArrayList<>();
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setInt(1, id);

            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                EquipamentoTrocado equipamento = new EquipamentoTrocado(rs.getString("sn"),
                        rs.getString("equipamento"), rs.getFloat("custo_real"),
                        rs.getInt("id_servico"));
                equipamentos.add(equipamento);
            }

            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }

        return equipamentos;
    }

    public static int getQuantidadeEquipamentos(int id) {
        int qnt = 0;
        String SQL = "SELECT count(*) FROM equip_trocados WHERE id_servico = ?";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                qnt = rs.getInt(1);
            }
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return qnt;
    }

    public static int getQuantidadeEquipamentos(int id, String busca) {
        int qnt = 0;
        String SQL = "SELECT count(*) FROM equip_trocados WHERE id_servico = ? "
                + "and sn LIKE '%"+busca+"%' or equipamento LIKE '%"+busca+"%' or custo_real LIKE '%"+busca+"%' or nota LIKE '%"+busca+"%'";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                qnt = rs.getInt(1);
            }
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return qnt;
    }
    
    public static float getCustoEquipamentos(int id) {
        float total = 0;
        String SQL = "SELECT sum(custo_real) FROM equip_trocados WHERE id_servico = ?";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setFloat(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                total = rs.getFloat(1);
            }
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    
    public static float getCustoEquipamentos(int id, String busca) {
        float total = 0;
        String SQL = "SELECT sum(custo_real) FROM equip_trocados WHERE id_servico = ? "
                + "and sn LIKE '%"+busca+"%' or equipamento LIKE '%"+busca+"%' or custo_real LIKE '%"+busca+"%' or nota LIKE '%"+busca+"%'";
        try {
            PreparedStatement pstm = ParearConexao.conectar().prepareStatement(SQL);
            pstm.setFloat(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                total = rs.getFloat(1);
            }
            ParearConexao.fechar();
        } catch (SQLException ex) {
            Logger.getLogger(DaoServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

}
