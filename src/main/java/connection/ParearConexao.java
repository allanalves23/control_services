package connection;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParearConexao {

    private static Connection connection;

    public static Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://" + file.ConfigServidor.lerIpServer()+ "/controle_servicos", file.ConfigServidor.lerUsuario(), file.ConfigServidor.lerSenha());
            return connection;
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver JDBC não encontrado na aplicação - classe: " + ex);
        } catch (SQLException ex) {
            Logger.getLogger(ParearConexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Connection conectarSemBanco() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://" + file.ConfigServidor.lerIpServer(), file.ConfigServidor.lerUsuario(), file.ConfigServidor.lerSenha());
            return connection;
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver JDBC não encontrado na aplicação - classe: " + ex);
        } catch (SQLException ex) {
            System.out.println("Erro ao adquirir a conexão com o banco - classe: " + ex.getErrorCode() + " - " +ex);
        }
        return null;
    }

    public static void fechar() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ParearConexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
