package file;

public class Encripter {


    public static String encriptar(String password) {
        password = password.toUpperCase();
        String senha = "";
        for (int i = 0; i < password.length(); i++) {
            if (password.charAt(i) != ' ') {
                senha += (char) ((password.charAt(i) - 65 + 3 % 26 + 65));
            } else {
                senha += " ";
            }
        }

        return senha;
    }

    public static String decifrar(String senha_encripada) {
        String password = "";
        for (int i = 0; i < senha_encripada.length(); i++) {
            if (senha_encripada.charAt(i) != ' ') {
                password += (char) ((senha_encripada.charAt(i) - 65 - 3) % 26 + 65);
            } else {
                password += " ";
            }
        }
        return password;
    }

}
