/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class Tema {
    static String nome_arq = "cfg/theme.dat";
    
    public static boolean gravarTema(String tema){
        FileOutputStream file = null;
        try {
            Pasta.verificarDiretorioCFG();
            file = new FileOutputStream(nome_arq);
            ObjectOutputStream obj = new ObjectOutputStream(file);
            obj.writeObject(tema);
            obj.close();
            file.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(Tema.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public static String obterTema(){
        if(new File("cfg").exists() && new File("cfg/theme.dat").exists()){
            try {
                ObjectInputStream obj = new ObjectInputStream(new FileInputStream(new File("cfg/theme.dat")));
                String tema = obj.readObject().toString();
                return tema;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Tema.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(Tema.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Nimbus";
        }else{
            return "Nimbus";
        }
    }
}
