/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allan
 */
public class ConfigServidor {

    public static boolean alterarConfigs(String ip, String usuario, String senha) {
        Properties propriedades = new Properties();
        File file = new File("cfg/config.properties");
        try {
            FileOutputStream f_out = new FileOutputStream(file);
            FileInputStream f_in = new FileInputStream(file);
            propriedades.load(f_in);
            propriedades.setProperty("IP_SERVER", ip);
            propriedades.setProperty("USER", usuario);
            propriedades.setProperty("PASSWORD", senha);
            propriedades.store(f_out, "CONTROLADOR DE SERVIÇOS");
            f_in.close();
            f_out.close();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static String lerIpServer(){
        Properties propriedades = new Properties();
        File file = new File("cfg/config.properties");
        
        if(file.exists()){
            try {
                FileInputStream f_in = new FileInputStream(file);
                propriedades.load(f_in);
                f_in.close();
                return propriedades.getProperty("IP_SERVER");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }else{
            return null;
        }
    }
    
    public static String lerUsuario(){
        Properties propriedades = new Properties();
        File file = new File("cfg/config.properties");
        
        if(file.exists()){
            try {
                FileInputStream f_in = new FileInputStream(file);
                propriedades.load(f_in);
                f_in.close();
                return propriedades.getProperty("USER");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }else{
            return null;
        }
    }
    
    
    public static String lerSenha(){
        Properties propriedades = new Properties();
        File file = new File("cfg/config.properties");
        
        if(file.exists()){
            try {
                FileInputStream f_in = new FileInputStream(file);
                propriedades.load(f_in);
                f_in.close();
                return Encripter.decifrar(propriedades.getProperty("PASSWORD"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }else{
            return null;
        }
    }
    
    
    
    public static void inicializarConfig(){
        Properties propriedades = new Properties();
        File file = new File("cfg/config.properties");
        if(!file.exists()){
            try {
                FileOutputStream f_out = new FileOutputStream("cfg/config.properties");
                propriedades.setProperty("IP_SERVER", "127.0.0.1");
                propriedades.setProperty("USER", "root");
                propriedades.setProperty("PASSWORD", "");
                propriedades.store(f_out, "teste");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ConfigServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
