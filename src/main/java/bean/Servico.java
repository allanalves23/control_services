/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.sql.Date;

/**
 *
 * @author Allan
 */
public class Servico {

    private String tipo_servico;
    private Date data_recebimento;
    private Date data_devolucao;
    private int prioridade;
    private float valor;
    private String descricao;
    private int nota_servico;
    private String cpf_cliente;
    private int id;
    //---------------------------------------------------------------------//
    private String nome_cliente;

    public Servico() {
    }

    public Servico(String tipo_servico, Date data_recebimento, int prioridade, float valor, String descricao, String cpf_cliente) {
        this.tipo_servico = tipo_servico;
        this.data_recebimento = data_recebimento;
        this.prioridade = prioridade;
        this.valor = valor;
        this.descricao = descricao;
        this.cpf_cliente = cpf_cliente;
    }
    

    public Servico(String tipo_servico, Date data_recebimento, Date data_devolucao, int prioridade, float valor, String descricao) {
        this.tipo_servico = tipo_servico;
        this.data_recebimento = data_recebimento;
        this.data_devolucao = data_devolucao;
        this.prioridade = prioridade;
        this.valor = valor;
        this.descricao = descricao;
    }

    public Servico(String tipo_servico, Date data_recebimento, Date data_devolucao, float valor, String descricao, int nota_servico, String cpf_cliente) {
        this.tipo_servico = tipo_servico;
        this.data_recebimento = data_recebimento;
        this.data_devolucao = data_devolucao;
        this.valor = valor;
        this.descricao = descricao;
        this.nota_servico = nota_servico;
        this.cpf_cliente = cpf_cliente;
    }

    public Servico(String tipo_servico, Date data_recebimento, Date data_devolucao, int prioridade, float valor, String descricao, int nota_servico, String cpf_cliente, int id, String nome_cliente) {
        this.tipo_servico = tipo_servico;
        this.data_recebimento = data_recebimento;
        this.data_devolucao = data_devolucao;
        this.prioridade = prioridade;
        this.valor = valor;
        this.descricao = descricao;
        this.nota_servico = nota_servico;
        this.cpf_cliente = cpf_cliente;
        this.id = id;
        this.nome_cliente = nome_cliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_cliente() {
        return nome_cliente;
    }

    public void setNome_cliente(String nome_cliente) {
        this.nome_cliente = nome_cliente;
    }

    public String getTipo_servico() {
        return tipo_servico;
    }

    public void setTipo_servico(String tipo_servico) {
        this.tipo_servico = tipo_servico;
    }

    public Date getData_recebimento() {
        return data_recebimento;
    }

    public void setData_recebimento(Date data_recebimento) {
        this.data_recebimento = data_recebimento;
    }

    public Date getData_devolucao() {
        return data_devolucao;
    }

    public void setData_devolucao(Date data_devolucao) {
        this.data_devolucao = data_devolucao;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getNota_servico() {
        return nota_servico;
    }

    public void setNota_servico(int nota_servico) {
        this.nota_servico = nota_servico;
    }

    public String getCpf_cliente() {
        return cpf_cliente;
    }

    public void setCpf_cliente(String cpf_cliente) {
        this.cpf_cliente = cpf_cliente;
    }

}
