/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.sql.Date;

/**
 *
 * @author Allan
 */
public class NotaCompra {

    private String registro_compra;
    private float valor_materia;
    private Date data_compra;
    private String modo_pagamento;
    //------------------------------------------------------------------------//
    private String equipamento;
    private String sn;

    public NotaCompra() {
    }

    public NotaCompra(String registro_compra, float valor_materia, Date data_compra, String modo_pagamento) {
        this.registro_compra = registro_compra;
        this.valor_materia = valor_materia;
        this.data_compra = data_compra;
        this.modo_pagamento = modo_pagamento;
    }

    public NotaCompra(String registro_compra) {
        this.registro_compra = registro_compra;
    }

    public NotaCompra(String registro_compra, float valor_materia, Date data_compra, String modo_pagamento, String equipamento, String sn) {
        this.registro_compra = registro_compra;
        this.valor_materia = valor_materia;
        this.data_compra = data_compra;
        this.modo_pagamento = modo_pagamento;
        this.equipamento = equipamento;
        this.sn = sn;
    }

    public String getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(String equipamento) {
        this.equipamento = equipamento;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getRegistro_compra() {
        return registro_compra;
    }

    public void setRegistro_compra(String registro_compra) {
        this.registro_compra = registro_compra;
    }

    public float getValor_materia() {
        return valor_materia;
    }

    public void setValor_materia(float valor_materia) {
        this.valor_materia = valor_materia;
    }

    public Date getData_compra() {
        return data_compra;
    }

    public void setData_compra(Date data_compra) {
        this.data_compra = data_compra;
    }

    public String getModo_pagamento() {
        return modo_pagamento;
    }

    public void setModo_pagamento(String modo_pagamento) {
        this.modo_pagamento = modo_pagamento;
    }

}
