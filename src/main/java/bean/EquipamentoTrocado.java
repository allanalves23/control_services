/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Allan
 */
public class EquipamentoTrocado {
    
    private String sn;
    private String equipamento;
    private float custo_real;
    private int id_servico;
    private String nota_servico;
    private NotaCompra nota_compra;

    public EquipamentoTrocado(String sn, String equipamento, float custo_real, int id_servico, NotaCompra nota_compra) {
        this.sn = sn;
        this.equipamento = equipamento;
        this.custo_real = custo_real;
        this.id_servico = id_servico;
        this.nota_compra = nota_compra;
    }

    public NotaCompra getNota_compra() {
        return nota_compra;
    }

    public void setNota_compra(NotaCompra nota_compra) {
        this.nota_compra = nota_compra;
    }

    public EquipamentoTrocado(String sn, String equipamento, float custo_real,int id_servico) {
        this.sn = sn;
        this.equipamento = equipamento;
        this.custo_real = custo_real;
        this.id_servico = id_servico;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(String equipamento) {
        this.equipamento = equipamento;
    }

    public float getCusto_real() {
        return custo_real;
    }

    public void setCusto_real(float custo_real) {
        this.custo_real = custo_real;
    }

    public int getId_servico() {
        return id_servico;
    }

    public void setId_servico(int id_servico) {
        this.id_servico = id_servico;
    }

    public String getNota_servico() {
        return nota_servico;
    }

    public void setNota_servico(String nota_servico) {
        this.nota_servico = nota_servico;
    }
    
    
    public EquipamentoTrocado() {
    }
    
}
